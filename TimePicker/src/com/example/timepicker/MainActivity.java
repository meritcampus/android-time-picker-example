package com.example.timepicker;

import java.util.Calendar;

import android.app.Activity;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.TimePicker;

public class MainActivity extends Activity implements OnClickListener {
	// declare  the variables
	TextView tvDisplayTime;
	Button btnChangeTime;
	Calendar dateTimeInfo;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		// get current time info using Calander.getInstance
		dateTimeInfo = Calendar.getInstance();
		btnChangeTime = (Button) findViewById(R.id.btnChangeTime);
		tvDisplayTime = (TextView) findViewById(R.id.tvTime);
		// Set onclick listener to button, for this we should implement OnClickListener Interface and onClick method. 
		btnChangeTime.setOnClickListener(this);
		setTimeToTextView();
	}

	//We are setting time information to display time text view.
	public void setTimeToTextView() {
		tvDisplayTime.setText(new StringBuilder().append(dateTimeInfo.get(Calendar.HOUR_OF_DAY)).append(":").append(dateTimeInfo.get(Calendar.MINUTE)).append(":").append(dateTimeInfo.SECOND));
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		return new TimePickerDialog(this, timePickerListener,dateTimeInfo.get(Calendar.HOUR_OF_DAY),dateTimeInfo.get(Calendar.MINUTE), false);
	}

	private TimePickerDialog.OnTimeSetListener timePickerListener = new TimePickerDialog.OnTimeSetListener() {
		public void onTimeSet(TimePicker view, int selectedHour,int selectedMinute) {
			dateTimeInfo.set(Calendar.HOUR, selectedHour);
			dateTimeInfo.set(Calendar.MINUTE, selectedMinute);
			setTimeToTextView();
		}
	};
	//onClick called when a view has been clicked. 
	@Override
	public void onClick(View view) {
		if (view.getId() == R.id.btnChangeTime)
			showDialog(R.id.btnChangeTime);
	}
}